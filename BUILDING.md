## Linux
### Dependencies
| Distro | Packages |
| ------ | ------ |
| Ubuntu | build-essential cmake libuv1-dev libssl-dev libhwloc-dev |
| Arch   | base-devel cmake libuv libmicrohttpd openssl libhwloc |
### Commands
```bash
mkdir build
cd build
cmake ..
make -j$(nproc)
```

## Windows
https://xmrig.com/docs/miner/build/windows

